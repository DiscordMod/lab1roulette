import java.util.Scanner;
public class Roulette {

    public static void main(String[] args)
    {
        Scanner reader = new Scanner(System.in);
        RouletteWheel wheel = new RouletteWheel();
        int money = 1000;
        boolean game = true;

        while(game){
            System.out.println("Would you like to make a bet? 1 for yes, 2 for no");
            boolean betMoney = true;

            int bet = reader.nextInt();
            int value = 0;

            if( bet == 1 ){
                
                 System.out.println("How much money are you betting?");
                
                 while(betMoney){
                    value = reader.nextInt();
                    if(value < 0 || value > money){
                        System.out.println("Invalid input, try again!");
                    }else{
                        betMoney = false;
                    }
                }
                System.out.println("What number are you betting?");
                int numBet = reader.nextInt();
                wheel.spin();
                if(numBet == wheel.getValue()){
                    System.out.println("The wheel shows: " + wheel.getValue());
                    money = money + (numBet - 1)*value;
                    System.out.println("Your money balance is: " + money);
                }else{
                    System.out.println("The wheel shows: " + wheel.getValue());
                    money = money - value;
                    System.out.println("Your money balance is: " + money);
                }


            }else{
                game = false;
            }
            if(money < 0){
                System.out.println("You're broke!!!!! Oh no!!!!");
                 game = false;
            }      
        }
    }
}



